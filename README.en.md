# 仿探探 即时通讯 聊天 社交 陌生人交友 包含前后端全套源码 三端通用

#### Description
双向喜欢后配对聊天的交友app全套程序，后端基于java ssm 架构，缓存使用redis，即时通讯使用 websocket+netty,前端基于uniapp ,适配 app，小程序，h5端，即时通讯， 聊天 ，社区， 个人中心，编辑资料，已对接微信公众号支付，内容合法性，人脸检测等，定位使用高德地图，用户所发文字和图片接入百度内容检测api，对非法内容进行检测，和识别用户上传图片是否为真人相册等

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
